/***************************************
Enter the maxium number of objects : 4
Enter the weights : 
Weight 1: 2
Weight 2: 1
Weight 3: 3
Weight 4: 2

Enter the profits : 
Profit 1: 12
Profit 2: 10
Profit 3: 20
Profit 4: 15

Enter the maximum capacity : 5

Profit Matrix
     0     0     0     0     0     0
     0     0    12    12    12    12
     0    10    12    22    22    22
     0    10    12    22    30    32
     0    10    15    25    30    37

Items selected for the Knapsack are : 1 2 4 
Total Profit earned is 37

***************************************/

