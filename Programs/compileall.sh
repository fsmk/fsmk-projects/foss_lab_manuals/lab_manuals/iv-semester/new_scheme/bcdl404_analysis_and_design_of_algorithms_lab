#!/bin/bash

# Iterate over folders P01 to P12
for folder in P{01..12}; do
    echo "Looking in folder $folder:"
    # Check if the folder exists
    if [ -d "$folder" ]; then
        # move to the folder
        cd "$folder"
        if [ "$folder" == "P03" ]; then
        	g++ 03AFloyd.cpp -o P03A.x
        	g++ 03BWarshall.cpp -o P03B.x
        	cd ..
    	else
        	fname="$folder"".x"
        	g++ *.cpp -o $fname
        	cd ..
        fi	
    else
        echo "Folder $folder does not exist."
    fi
    echo ""
done

