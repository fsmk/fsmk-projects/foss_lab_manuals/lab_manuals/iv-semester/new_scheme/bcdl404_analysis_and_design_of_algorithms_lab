Enter the number of items : 4
Enter Knapsack capacity : 5
Enter profit : 12
Enter weight : 2
Enter profit : 10
Enter weight : 1
Enter profit : 20
Enter weight : 3
Enter profit : 15
Enter weight : 2

DISCRETE KNAPSACK GREEDY SOLUTION
Item	Weight	Profit	Fraction_Chosen
   1	     1	    10	     1
   2	     2	    15	     1
   3	     3	    20	     0
   4	     2	    12	     0

Total Profit Earned : 25.00

CONTINUOUS KNAPSACK GREEDY SOLUTION
Item	Weight	Profit	Fraction Chosen
   1	     1	    10	  1.00
   2	     2	    15	  1.00
   3	     3	    20	  0.67
   4	     2	    12	  0.00

Total Profit Earned : 38.33

